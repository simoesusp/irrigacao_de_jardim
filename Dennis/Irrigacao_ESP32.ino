// Programa : Controle de Irriga��o
// Sonar HC-SR04
// Sensor Umidade YL69
// Sensor Luminosidade LDR
// Sensor de temperatura DS18B20
// Aciona V�lvula de �gua de Maquina de lavar via Rel�
// Aciona bomba 12V de Limpador de Parabrisa de Carro via Rel�
// Servo Motor no pino 9
// Autor : Prof. Simoes

#include "prevTempo.h"  // Nossa pr�pria biblioteca para conversar com o ESP32
PrevTempo prevTempo;
 
#include <OneWire.h>
#include <DallasTemperature.h>

// Sensor Umidade YL69
#define pino_umidade A5

// Sensor Luminosidade LDR
#define pino_luminosidade A4

// Porta do pino de sinal do DS18B20
#define ONE_WIRE_BUS 5

// Pino do Rele' que liga irriga��o
#define pino_rele 13

// Servo Motor no pino 9
#include <Servo.h>
Servo myservo;  // Cria objeto Servo para controlar o Servo-motor
 
// Define uma instancia do oneWire para comunicacao com o sensor
OneWire oneWire(ONE_WIRE_BUS);

DallasTemperature sensors(&oneWire);
DeviceAddress sensor1;


 
// Armazena temperaturas minima e maxima
float tempMin = 999;
float tempMax = 0;

// Umidade
int umidade = 0;
int luminosidade = 0;
 

 

void setup(void)
{
  pinMode(pino_rele, OUTPUT);
  digitalWrite(pino_rele, HIGH);    // Meu Rel� liga comm ZERO !!

  myservo.attach(9);  // Conecta o Servo no pino 9
  
  Serial.begin(9600); // Define velocidade da Porta Serial para Terminal de Monitoramento
  sensors.begin();  // Inicializa Sensor de Temperatura
  // Localiza e mostra enderecos dos sensores
  Serial.println("Localizando sensores DS18B20... ");
  Serial.print("Foram encontrados ");
  Serial.print(sensors.getDeviceCount(), DEC);
  Serial.println(" sensores.");
  if (!sensors.getAddress(sensor1, 0)) 
     Serial.println("Sensores nao encontrados !"); 
  // Mostra o endereco do sensor encontrado no barramento
  Serial.print("Endereco sensor: ");
  mostra_endereco_sensor(sensor1);
  Serial.println();
  Serial.println(); 
}
 
void mostra_endereco_sensor(DeviceAddress deviceAddress)
{
  for (uint8_t i = 0; i < 8; i++)
  {
    // Adiciona zeros se necess�rio
    if (deviceAddress[i] < 16) Serial.print("0");
    Serial.print(deviceAddress[i], HEX);
  }
}

// Sonar HC-SR04
long readUltrasonicDistance(int triggerPin, int echoPin)
{
  pinMode(triggerPin, OUTPUT);  // Clear the trigger
  digitalWrite(triggerPin, LOW);
  delayMicroseconds(2);
  // Sets the trigger pin to HIGH state for 10 microseconds
  digitalWrite(triggerPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(triggerPin, LOW);
  pinMode(echoPin, INPUT);
  // Reads the echo pin, and returns the sound wave travel time in microseconds
  int leitura_sonar = pulseIn(echoPin, HIGH);
  leitura_sonar = 0.01723 * leitura_sonar;
  if(leitura_sonar < 11) leitura_sonar = 400;
  return (leitura_sonar);
}

void loop()
{
  // Le a informacao do sensor
  sensors.requestTemperatures();
  float tempC = sensors.getTempC(sensor1) / 2; // Meu sensor est� com defeito, ent�o estou dividindo por 2 a leitura...
  // Atualiza temperaturas minima e maxima
  if (tempC < tempMin)         tempMin = tempC;
  else if (tempC > tempMax)    tempMax = tempC;
  
  // Mostra dados no serial monitor
  Serial.print("Temp C: ");
  Serial.print(tempC);
  Serial.print(" Min : ");
  Serial.print(tempMin);
  Serial.print(" Max : ");
  Serial.println(tempMax);

  // Le sensor de umidade
  umidade = analogRead(pino_umidade); // Valor entre 0 e 1023 - Quanto mais umido, menor o valor
  Serial.print("Umidade : ");
  Serial.print(umidade);
  
  // Le sensor de luminosidade - LDR
  luminosidade = analogRead(pino_luminosidade); // Valor entre 0 e 1023 - Quanto mais luz, maior o valor
  Serial.print("    Luminosidade : ");
  Serial.println(luminosidade);

  //prevTempo.atualiza();  // Pega a previs�o do tempo do ESP32 via segunda porta serial
  while(prevTempo.temperatura == 100) { // Se informa��es n�o est�o prontas...
    prevTempo.atualiza();       // Fica atualizando at� receber informa��es validas
    Serial.print("Temp: " + String(prevTempo.temperatura) + "�C");
    Serial.print("  Wind: " + String(prevTempo.vento) + "km/h");
    Serial.println("  Rain: " + String(prevTempo.chuvaAmanha) + "mm");
    delay(1000);  // Espera 1 segundo
  }

  

  int tempMedia = (tempC + prevTempo.temperatura) /2; // Calcua a m�dia da temperatura Local + Internet
  
  int tempo = 0;
  if (umidade > 800)  {   // Se umidade for menor que 800 � porque choveu recentemente, ent�o n�o irriga
    
    if(tempMedia > 30) tempo = tempo + 60;        // Se Temperatura for alta, acrescenta 60seg no tempo de irriga��o
    else if(tempMedia < 10) tempo = tempo + 20;   // Se Temperatura for baixa, acrescenta 20seg no tempo de irriga��o
    else tempo = tempo + 40;                  // Se Temperatura for media, acrescenta 40seg no tempo de irriga��o

    if(luminosidade > 250) tempo = tempo + 60;        // Se luminosidade for alta, acrescenta 60seg no tempo de irriga��o
    else if(luminosidade < 190) tempo = tempo + 20;   // Se luminosidade for baixa, acrescenta 20seg no tempo de irriga��o
    else tempo = tempo + 40;                          // Se luminosidade for media, acrescenta 40seg no tempo de irriga��o

    if(prevTempo.vento > 50) tempo = tempo + 60;        // Se luminosidade for alta, acrescenta 60seg no tempo de irriga��o
    else if(prevTempo.vento < 10) tempo = tempo + 20;   // Se luminosidade for baixa, acrescenta 20seg no tempo de irriga��o
    else tempo = tempo + 40;                          // Se luminosidade for media, acrescenta 40seg no tempo de irriga��o

    if(prevTempo.chuvaAmanha > 3) tempo = 0;        // Se luminosidade for alta, acrescenta 60seg no tempo de irriga��o
    else if(prevTempo.chuvaAmanha < 1) tempo = tempo + 40;   // Se luminosidade for baixa, acrescenta 20seg no tempo de irriga��o
    else tempo = tempo + 20;                          // Se luminosidade for media, acrescenta 40seg no tempo de irriga��o
  }

  Serial.print("Tempo : ");
  Serial.println(tempo);
    
  if((tempo > 0) && (readUltrasonicDistance(7, 6) > 200))   digitalWrite(pino_rele, LOW);   // Meu Rel� liga comm ZERO !!

  int servo_passo = 30;

  while(tempo > 0) {
    tempo--;
    int sonar = readUltrasonicDistance(7, 6);
    if(sonar < 200) tempo = 0; // Se alguem chegar a 2m de dist�ncia do Jardim para de irrigar
    Serial.print("Sonar: ");
    Serial.print(sonar);
    Serial.print(" Tempo : ");
    Serial.println(tempo);
    myservo.write(servo_passo);                  // sets the servo position according to the scaled value
    servo_passo = servo_passo + 10;
    if(servo_passo > 110) servo_passo = 0;
    delay(1000);  // Espera 1 segundo
  }
  digitalWrite(pino_rele, HIGH);    // Meu Rel� liga comm ZERO !!

  // Espera 24h
  for(int h=0;h<24;h++)
    for(int m=0;m<60;m++)
      for(int s=0;s<60;s++) {
        Serial.print("Hora: ");
        Serial.print(h);
        Serial.print(" Min: ");
        Serial.print(m);
        Serial.print(" seg : ");
        Serial.println(s);
        delay(1000);  // Espera 1 segundo
    }
     
}
