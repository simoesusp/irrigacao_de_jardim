afgadgdafgdfg


# Previsao do Tempo
- https://wttr.in/sao_carlos

- Pra pegar somente a info que se quer, usar?
  - curl https://wttr.in/sao_carlos?format=%t.%w
  - Isso pega somente a temperatura e o Vento: +25oC,<-14Km/h

- A lista dos formatos esta' em: github.com/chubin/wttr.in

## Exemplos:

- To specify your own custom output format, use the special %-notation:
```
    c    Weather condition,
    C    Weather condition textual name,
    x    Weather condition, plain-text symbol,
    h    Humidity,
    t    Temperature (Actual),
    f    Temperature (Feels Like),
    w    Wind,
    l    Location,
    m    Moon phase 🌑🌒🌓🌔🌕🌖🌗🌘,
    M    Moon day,
    p    Precipitation (mm/3 hours),
    P    Pressure (hPa),
    u    UV index (1-12),

    D    Dawn*,
    S    Sunrise*,
    z    Zenith*,
    s    Sunset*,
    d    Dusk*,
    T    Current time*,
    Z    Local timezone.

(*times are shown in the local timezone)
So, these two calls are the same:

    $ curl wttr.in/London?format=3
    London: ⛅️ +7⁰C
    $ curl wttr.in/London?format="%l:+%c+%t\n"
    London: ⛅️ +7⁰C

```

# Servidor de Real Time

- https://lastminuteengineers.com/esp32-ntp-server-date-time-tutorial/

- Usar o Server: http://a.ntp.br/


```
Previsão do tempo para: sao_carlos

     \  /       Parcialmente nublado
   _ /"".-.     +28(27) °C     
     \_(   ).   ↙ 6            
     /(___(__)  10             
                0.0            
                                                       ┌─────────────┐                                                       
┌──────────────────────────────┬───────────────────────┤  Qua 24 Ago ├───────────────────────┬──────────────────────────────┐
│             Manhã            │           Meio-dia    └──────┬──────┘      Tarde            │             Noite            │
├──────────────────────────────┼──────────────────────────────┼──────────────────────────────┼──────────────────────────────┤
│     \   /     Sol            │     \   /     Sol            │    \  /       Parcialmente n…│     \   /     Céu limpo      │
│      .-.      17 °C          │      .-.      25 °C          │  _ /"".-.     +27(26) °C     │      .-.      20 °C          │
│   ― (   ) ―   ← 9-12         │   ― (   ) ―   ↙ 14-17        │    \_(   ).   ↙ 2-3          │   ― (   ) ―   ↖ 7-15         │
│      `-’      10             │      `-’      10             │    /(___(__)  10             │      `-’      10             │
│     /   \     0.0  | 0%      │     /   \     0.0  | 0%      │               0.0  | 0%      │     /   \     0.0  | 0%      │
└──────────────────────────────┴──────────────────────────────┴──────────────────────────────┴──────────────────────────────┘
                                                       ┌─────────────┐                                                       
┌──────────────────────────────┬───────────────────────┤  Qui 25 Ago ├───────────────────────┬──────────────────────────────┐
│             Manhã            │           Meio-dia    └──────┬──────┘      Tarde            │             Noite            │
├──────────────────────────────┼──────────────────────────────┼──────────────────────────────┼──────────────────────────────┤
│     \   /     Sol            │     \   /     Sol            │     \   /     Céu limpo      │     \   /     Céu limpo      │
│      .-.      19 °C          │      .-.      27 °C          │      .-.      +29(28) °C     │      .-.      24 °C          │
│   ― (   ) ―   ↙ 18-24        │   ― (   ) ―   ↙ 18-20        │   ― (   ) ―   ↑ 4-5          │   ― (   ) ―   ↖ 6-12         │
│      `-’      10             │      `-’      10             │      `-’      10             │      `-’      10             │
│     /   \     0.0  | 0%      │     /   \     0.0  | 0%      │     /   \     0.0  | 0%      │     /   \     0.0  | 0%      │
└──────────────────────────────┴──────────────────────────────┴──────────────────────────────┴──────────────────────────────┘
                                                       ┌─────────────┐                                                       
┌──────────────────────────────┬───────────────────────┤  Sex 26 Ago ├───────────────────────┬──────────────────────────────┐
│             Manhã            │           Meio-dia    └──────┬──────┘      Tarde            │             Noite            │
├──────────────────────────────┼──────────────────────────────┼──────────────────────────────┼──────────────────────────────┤
│     \   /     Sol            │     \   /     Sol            │     \   /     Céu limpo      │     \   /     Céu limpo      │
│      .-.      21 °C          │      .-.      +31(30) °C     │      .-.      +30(29) °C     │      .-.      +23(24) °C     │
│   ― (   ) ―   ↙ 19-25        │   ― (   ) ―   ↓ 18-20        │   ― (   ) ―   → 7-8          │   ― (   ) ―   ↖ 4-8          │
│      `-’      10             │      `-’      10             │      `-’      10             │      `-’      10             │
│     /   \     0.0  | 0%      │     /   \     0.0  | 0%      │     /   \     0.0  | 0%      │     /   \     0.0  | 0%      │
└──────────────────────────────┴──────────────────────────────┴──────────────────────────────┴──────────────────────────────┘
Localização: São Carlos, Microrregião de São Carlos, Mesorregião de Araraquara, SP, Região Sudeste, Brasil [-22.0183367,-47.890932]

```

